const {
    TIMEOUTS,
    PUPPETEER,
} = require('../../consts');

module.exports = {
    TIMEOUTS,
    PUPPETEER,
};
